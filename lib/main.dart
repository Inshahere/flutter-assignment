import 'package:flutter/material.dart';
import 'text-controller.dart';
import 'text-display.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String changeableText = 'I\'ll Change Soon';

  void _changeText() {
    setState(() {
      changeableText = 'Bingo I just changed';
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Flutter Assignment'),
        ),
        body: Column(
          children: [
            DisplayText(displayText: changeableText),
            Controller(changeController: _changeText),
          ],
        ),
      ),
    );
  }
}
