import 'package:flutter/material.dart';

class Controller extends StatelessWidget {
  final Function changeController;

  Controller({this.changeController});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text('Change Me'),
      onPressed: changeController,
    );
  }
}
