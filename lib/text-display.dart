import 'package:flutter/material.dart';

class DisplayText extends StatelessWidget {
  final String displayText;
  DisplayText({@required this.displayText});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: double.infinity,
      padding: EdgeInsets.only(top: 15),
      child: Text(displayText),
    );
  }
}
